import sys
from fabric import Connection, task
from invoke import Responder
from fabric.config import Config
import os

PROJECT_NAME = "beekin "
PROJECT_PATH = "/srv/beekin/"
REPO_URL = "/srv/beekin/"


def get_connection(ctx):
    try:
        with Connection(ctx.host, ctx.user) as conn:
            return conn
    except Exception as e:
        return None

@task
def development(ctx):
    ctx.user = os.environ["username"]
    ctx.host = os.environ["server_ip"]

# check if file exists in directory(list)
def exists(file, dir):
    return file in dir

@task
def checkout(ctx, branch=None):
    if branch is None:
        sys.exit("branch name is not specified")
    print("branch-name: {}".format(branch))

    if isinstance(ctx, Connection):
        conn = ctx
    else:
        conn = get_connection(ctx)
    with conn.cd(REPO_URL):
        fetch_all = conn.run('git fetch --all')
        conn.run('git reset --hard origin/{}'.format(branch))


@task
def migrate(ctx):
    if isinstance(ctx, Connection):
        conn = ctx
    else:
        conn = get_connection(ctx)
    with conn.cd(REPO_URL):
        conn.run("venv/bin/python manage.py migrate")

@task
def requirements(ctx):
    if isinstance(ctx, Connection):
        conn = ctx
    else:
        conn = get_connection(ctx)
    with conn.cd(REPO_URL):
        conn.run("cat requirements.txt | xargs -n 1 venv/bin/pip install --no-cache-dir")

@task
def settings(ctx):
    if isinstance(ctx, Connection):
        conn = ctx
    else:
        conn = get_connection(ctx)
    with conn.cd(REPO_URL):
        conn.run('cp src/settings/settings_dev.py src/settings/settings.py')

# supervisor tasks
@task
def start(ctx):
    if isinstance(ctx, Connection):
        conn = ctx
    else:
        conn = get_connection(ctx)
    conn.sudo("supervisorctl start all")


@task
def restart(ctx):
    if isinstance(ctx, Connection):
        conn = ctx
    else:
        conn = get_connection(ctx)
    print("restarting supervisor...")
    conn.run("sudo supervisorctl restart all")

@task
def stop(ctx):
    if isinstance(ctx, Connection):
        conn = ctx
    else:
        conn = get_connection(ctx)
    conn.sudo("supervisorctl stop all")


@task
def status(ctx):
    if isinstance(ctx, Connection):
        conn = ctx
    else:
        conn = get_connection(ctx)
    conn.sudo("supervisorctl status")


# deploy task
@task
def deploy(ctx):
    conn = get_connection(ctx)
    if conn is None:
        sys.exit("Failed to get connection")
    
    with conn.cd(REPO_URL):
        print("checkout to dev branch...")
        checkout(conn, branch="master")
        print("installing requirements")
        requirements(conn)
        print("managing corresponding settings for server")
        settings(conn)
        print("migrating database....")
        migrate(conn)
        print("restarting the supervisor...")
        restart(conn)