# Beeking API Backend

A SCRAPER + REST API for Indeed Jobs built using Django Rest Framework

## Installation

### Requirements
- Python
- Django

    Complete list available in requirements.txt file

### Remote Server

Links mentioned in this documentation are mentioned with BASE_URL set to LOCALHOST. Kindly note that CI/CD job is set to this repo, and the project is well deployed under **167.99.47.159:8080**

### Quickstart
- Clone the repo.  
    ```bash
    git clone https://Nachi_med_ali@bitbucket.org/Nachi_med_ali/beekin_drf.git
    ```

- Inside the src folder, make a virtual environment and activate it 
    ```bash
    cd beekin_drf
    python -m venv env 
    source env/bin/activate
    ```

- Install requirements from requirements.txt
    ```
    pip install -r requirements.txt
    ```

- Makemigrations and migrate the project
    ```
    python manage.py makemigrations && python manage.py migrate
    ```

- Create a superuser
    ```
    python manage.py createsuperuser
    ```

- Runserver
    ```
    python manage.py runserver
    ```

- Unitests
    ```
    pytest
    ```

**Note: After running the server, you can use the api inside browser or you can use Postman to make api calls.**

# RESTAPI Docs
I have added `drf-yasg` for API documentation which can be accessed after running the backend server and going to following links:

Swagger UI docs:    http://127.0.0.1:8000/swagger/ OR REMOTE **http://167.99.47.159:8080/swagger/**

Redoc UI docs:  http://127.0.0.1:8000/redoc/ OR REMOTE **http://167.99.47.159:8080/redoc/**


## API

### Models

- Technology:
    - id: Technology ID(read only),
    - name: string(unique)


- Job:
    - id: Post id(read only),
    - created_at: datetime(read only),
    - indeed_link: string(unique),
    - title: string,
    - location: string,
    - description: text,
    - tech: MtM to Technology Model,
    - experience_required:string,
    - company_name: string,
    - posted_date: date(read only)
    - responsabilities:string



### Endpoints

Brief explanation of endpoints:

| Function                                                                                               | REQUEST    | Endpoint                                                | Authorization | form-data                                  |
|--------------------------------------------------------------------------------------------------------|------------|---------------------------------------------------------|---------------|-------------------------------------------|
| Create new technology                                                                                  | POST       | http://127.0.0.1:8000/api/v1/technologies/           | Not Required    | name                 |
| Returns list of all existing technology                                                                | GET        | http://127.0.0.1:8000/api/v1/technologies/           | Not Required    |                                           |
| Returns the detail of a technology                                                                     | GET        | http://127.0.0.1:8000/api/v1/technologies/{int:id}/  | Not Required    |                                           |
| Update the details of a technology                                                                     | PUT, PATCH | http://127.0.0.1:8000/api/v1/technologies/{int:id}/  | Not Required    |                                           |
| Delete a technology instance                                                                           | DELETE     | http://127.0.0.1:8000/api/v1/technologies/{int:id}/  | Not Required    |                                           |
|                                                                                                        |            |                                                         |               |                                           |
| Returns a list of all existing jobs                                                                    | GET        | http://127.0.0.1:8000/api/v1/jobs/                   | Not Required  |                                           |
| Returns the details of a JOB instance. Searches post using search paramater.                           | GET        | http://127.0.0.1:8000/api/v1/jobs/{int:id}/          | Not Required    |                                           |
|                                                                                                        |            |                                                         |               |                                           |
| Run Indeed scraper based on options dict                                                               | POST       | http://127.0.0.1:8000/api/v1/scraper/                | Not Required    |                                           |
| Returns the status of scraping job (progress, failed, success).                                        | POST        | http://127.0.0.1:8000/api/v1/job_progress/               | Not Required    |                                           |


### Curl Examples

For scraper API endpoint, a JSON dict needs to be sent in the POST request containing ***language*** and ***query*** keys (required)

Returns the job_id if success, else error message would be returned

Example

```bash
    curl -X POST 127.0.0.1:8000/api/v1/scraper/ \
    -H "Content-Type: application/json" \
    -d '{"query" : "test", "language" : "fr" }'
```

For Job status API endpoint, a JSON dict needs to be sent in the POST request containing ***job_id***  key (required)

Example

```bash
    curl -X POST 127.0.0.1:8000/api/v1/job_progress/ \
    -H "Content-Type: application/json" \
    -d '{"job_id" : "JOB_ID_RETURNED_IN_PREVIOUS_EXAMPLE" }'
```

Returns the status if JOB_ID is valid, else error message would be returned


### Filters, search and Pagination

As requested in assignement, API supports pagination and filters, also search field is defined for searching keywords

**Search**

Activated with "search" parameter, search_fields is limited to ***title*** Job field

```
    http://127.0.0.1:8000/api/v1/jobs/?search=node
```

**Filter**

Filters are activated for both fields ***title*** and ***tech*** Job fields. Kindly note that techs is MtM (Many To Many) field.

```
    http://127.0.0.1:8000/api/v1/jobs/?title=node&techs=python
```

## Ways to improve
- Allow authentification
- Include Scraping progress in API, that when user processed new scraping, gets the progress of the scraping job.  

