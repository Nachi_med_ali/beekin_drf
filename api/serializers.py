from rest_framework import serializers
from api.models import Job, Technology

class JobListSerializer(serializers.ModelSerializer):
    technologies = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Job
        fields = [
            "id",
            "indeed_link",
            "title",
            "location",
            "description",
            "technologies",
            "experience_required",
            "company_name",
            "responsabilities",
        ]

    def get_technologies(self, obj):
        return [tech.name for tech in obj.tech.all()]

class TechnologySerializer(serializers.ModelSerializer):

    class Meta:
        model = Technology
        fields = [
            "id",
            "name"
            ]











