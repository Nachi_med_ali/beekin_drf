from django.contrib import admin

from api.models import Technology, Job

@admin.register(Technology)
class TechnologyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    list_display = ('id', 'indeed_link', 'title', 'company_name', 'created_at')