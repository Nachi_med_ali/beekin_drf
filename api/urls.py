from django.urls import path
from rest_framework.routers import DefaultRouter
from api import views

app_name = "api"

router = DefaultRouter()

router.register('technologies', views.TechnologyViewSet , basename='techs')

urlpatterns = [
    path("jobs/", views.ListJobsAPIView.as_view(), name="list_jobs"),
    path("jobs/<int:pk>/", views.ListSingleJobAPIView.as_view(), name="list_specific_job"),
    path("scraper/", views.CreateScraperJobView.as_view(), name="setup_scraper_job"),

    path("job_progress/", views.WatchScraperJobView.as_view(), name="job_progress"),
    
]

urlpatterns += router.urls