import django_filters

from api.models import Job


class JobFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(field_name='title', lookup_expr='icontains')
    techs = django_filters.CharFilter(field_name='tech__name', lookup_expr='icontains')

    class Meta:
        model = Job 
        fields = ['title', "tech"]