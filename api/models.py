from django.db import models


class Technology(models.Model):
	name = models.CharField('Name', unique=True, max_length=100, blank=True)

	def __str__(self):
		return self.name

class Job(models.Model):
	"""
	Job Model
	"""
	class Meta:
		ordering = ['-pk']

	created_at = models.DateTimeField(auto_now_add=True)

	indeed_link = models.CharField('Indeed link', unique=True, max_length=200, blank=True)
	title = models.CharField('Title', max_length=100, blank=True)
	location = models.CharField('Location', max_length=60, blank=True)
	description = models.TextField('Job Description', blank=True)

	tech = models.ManyToManyField(Technology, related_name='technology')

	experience_required = models.CharField('Experience', max_length=255, blank=True)

	company_name = models.CharField('Company', max_length=100, blank=True)

	posted_date = models.DateField('Post Date', null=True, blank=True)
	responsabilities = models.TextField('Responsabilities',  blank=True)

	def __str__(self):
		return self.title