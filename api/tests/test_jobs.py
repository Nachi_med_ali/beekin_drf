import os
from django.urls import reverse
from django.test import override_settings

from src.factories import TechnologyFactory, JobFactory
from api.models import Job

from django.test import TestCase

from rest_framework.test import APIClient
from django.urls import reverse

import random
import string

char_set = string.ascii_uppercase + string.digits

TEST_JOB_DICT = {
    "name" : "django"
}

class TechnologyTests(TestCase):
    def test_paginate_filter_jobs(self):

        #Generating batch of 20 jobs
        JobFactory.create_batch(20)
        query_url = reverse('api:list_jobs')

        # Creating a new technology with API
        resp = self.client.get(query_url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.data["results"]), 15)

        # randomly editing 7 objects from the 20 created jobs
        edit_job_ids = [1,3,4,5,6,10]
        Job.objects.filter(id__in = edit_job_ids).update(title = "Accounter")

        # filtering with exact word
        resp = self.client.get(query_url+"?title=accounter")
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.data["results"]), 6)

        # filtering with word containg sort
        Job.objects.filter(id__in = edit_job_ids).update(title = "AccounterPOST")
        resp = self.client.get(query_url+"?title=accounter")
        self.assertEqual(len(resp.data["results"]), 6)

        # filtering based on tech
        tech_1 = TechnologyFactory()
        tech_2 = TechnologyFactory()

        jobs_tech_1 = Job.objects.filter(id__in = edit_job_ids[0:3])
        for job in jobs_tech_1:
            job.tech.set([tech_1])

        jobs_tech_2 = Job.objects.filter(id__in = edit_job_ids[3:5])
        for job in jobs_tech_2:
            job.tech.set([tech_2])

        # filtering by title and techs 
        resp = self.client.get(query_url+"?title=accounter&techs={}".format(tech_1.name))
        self.assertEqual(len(resp.data["results"]), 3)

        resp = self.client.get(query_url+"?title=accounter&techs={}".format(tech_2.name))
        self.assertEqual(len(resp.data["results"]), 2)

    def test_search_jobs(self):

        #Generating batch of 20 jobs
        JobFactory.create_batch(20)
        query_url = reverse('api:list_jobs')

        # Creating a new technology with API
        resp = self.client.get(query_url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.data["results"]), 15)

        # randomly editing 7 objects from the 20 created jobs
        edit_job_ids = [1,7,8,10]

        jobs = Job.objects.filter(id__in = edit_job_ids)
        for job in jobs:
            pre, pos = ''.join(random.sample(char_set*6, 6)), ''.join(random.sample(char_set*6, 6))
            job.title = pre+"KEYWORD"+pos
            job.save()

        # filtering by title and techs 
        resp = self.client.get(query_url+"?search=KEYWORD")
        self.assertEqual(len(resp.data["results"]), 4)

    def test_single_jobs(self):

        #Generating batch of 20 jobs
        JobFactory.create_batch(3)

        job = Job.objects.all().first()
        # Creating a new technology with API
        resp = self.client.get(reverse('api:list_specific_job', kwargs={'pk':job.id} ))
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data["id"], job.id)
        self.assertEqual(resp.data["title"], job.title)
        self.assertEqual(resp.data["indeed_link"], job.indeed_link)
        self.assertEqual(resp.data["location"], job.location)
        self.assertEqual(resp.data["description"], job.description)








