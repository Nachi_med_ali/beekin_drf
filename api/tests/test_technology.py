import os
from django.urls import reverse
from django.test import override_settings

from src.factories import TechnologyFactory
from api.models import Technology

from django.test import TestCase

from rest_framework.test import APIClient

TEST_TECHNOLOGY_DICT = {
    "name" : "django"
}

class TechnologyTests(TestCase):
    def test_add_new_technology(self):

        # Creating a new technology with API
        resp = self.client.post('/api/v1/technologies/', data=TEST_TECHNOLOGY_DICT, format = 'json')
        self.assertEqual(resp.status_code, 201)

        # Checking creation in DB
        techs = Technology.objects.all()
        self.assertEqual(techs.count(), 1)

    def test_add_new_redundant_technology(self):

        # Creating a new technology with API
        resp = self.client.post('/api/v1/technologies/', data=TEST_TECHNOLOGY_DICT, format = 'json')
        self.assertEqual(resp.status_code, 201)

        # Recreating a tech with same name
        resp = self.client.post('/api/v1/technologies/', data=TEST_TECHNOLOGY_DICT, format = 'json')
        self.assertEqual(resp.status_code, 400)

        # Creating a tech name with new name
        resp = self.client.post('/api/v1/technologies/', data={"name" : "python"}, format = 'json')
        self.assertEqual(resp.status_code, 201)

        # Checking creation in DB
        techs = Technology.objects.all()
        self.assertEqual(techs.count(), 2)

    def test_edit_delete_technology(self):

        # create tech object
        tech_obj = TechnologyFactory()

        tech_obj_from_db = Technology.objects.all()
        self.assertEqual(tech_obj_from_db.count(), 1)
        # Edition tech
        client = APIClient()
        resp = client.put("/api/v1/technologies/{}/".format(tech_obj.id), data={"name" : "python"}, format = 'json')
        self.assertEqual(resp.status_code, 200)

        tech_obj_from_db = Technology.objects.all().first()
        self.assertEqual(tech_obj_from_db.name, "python")

        # deleting Tech
        resp = client.delete('/api/v1/technologies/{}/'.format(tech_obj.id), format = 'json')
        self.assertEqual(resp.status_code, 204)
        tech_obj_from_db = Technology.objects.all()
        self.assertEqual(tech_obj_from_db.count(), 0)





