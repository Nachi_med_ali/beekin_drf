from rest_framework.permissions import AllowAny

from rest_framework.views import APIView
from rest_framework import viewsets

from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView
)

from api.pagination import JobLimitOffsetPagination
from api.models import Job, Technology
from api.serializers import (
    JobListSerializer,
    TechnologySerializer
)
from api.filters import (
		JobFilter
	)

from rest_framework import filters
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from django.http.response import JsonResponse

from src.tasks import scrapeIndeed
from celery.result import AsyncResult

class ListJobsAPIView(ListAPIView):
    """
    get:
        Returns a list of all existing posts
    """

    queryset = Job.objects.all()
    serializer_class = JobListSerializer
    permission_classes = (AllowAny, )
    pagination_class = JobLimitOffsetPagination
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filter_class = JobFilter
    search_fields = ('title', )
    lookup_field = "id"
    ordering_fields = ('id', 'title')

class ListSingleJobAPIView(RetrieveAPIView):
    """
    get:
        Returns a specific job by id if exists
    """

    queryset = Job.objects.all()
    serializer_class = JobListSerializer
    permission_classes = (AllowAny, )


class TechnologyViewSet(viewsets.ModelViewSet):
    """
    A viewset for Technology instances.
    """
    queryset = Technology.objects.all()
    serializer_class = TechnologySerializer
    permission_classes = (AllowAny, )
    

class CreateScraperJobView(APIView):
    """
    post:
        Creates a new scraper job.
    """

    permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        options = request.data
        if "query" not in options:
            return Response({"message" : "query is required"}, status=400)
        if "language" not in options:
            return Response({"message" : "language is required"}, status=400)
        else:
            if options["language"] not in ['en', 'fr', 'uk', 'de']:
                return Response({"message" : "specify a valid choise of language ['en', 'fr', 'uk', 'de']"}, status=400)


        celery_task = scrapeIndeed.delay(options)
        return Response({"task_id" : celery_task.id }, status=200)

class WatchScraperJobView(APIView):
    """
    post:
        Watch progress of scraper job based on job ID.
    """

    permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        options = request.data
        if "job_id" not in options:
            return Response({"message" : "job_id is required"}, status=400)

        try:
            job = AsyncResult(options["job_id"])
            data = job.result or job.state
            return Response({"result" : data }, status=200)
        except:
            return Response({"message" : "JOB ID not found" }, status=200)
