"""
Defining Indeed Scraper
"""

import requests
from bs4 import BeautifulSoup
import re
from datetime import datetime, timedelta
from api.models import Technology, Job
import time

from celery import current_task

BASE_URLS = {
	"fr" : "https://indeed.fr/",
	"en" : "https://indeed.com/",
	"uk" : "https://uk.indeed.com/",
	"de" : "https://de.indeed.com/"
}

# PAGE_URL = "http://indeed.fr/jobs?q=python&l=paris&r=25&limit=50"

MAX_QUERIES = 50
today = datetime.now().date()

DEFAULT_OPTIONS = {
	"language" : "fr",
	"query" : "python",
	"location" : "paris",
	"radius" : 25,
	"limit" : 10
}

techs = Technology.objects.all()

class IndeedParser:
	def __init__(self, options = {}, task_id = ""):
		self.links = []
		self.options = options if options else DEFAULT_OPTIONS
		self.base_url = BASE_URLS[self.options["language"]] if self.options["language"] in BASE_URLS else ""
		self.query_link = ""
		self.task_id = task_id
		self.jobs = []

	def generateMainLink(self):
		"""
		generate query URL based on options checked by user
		Args:
			options (dict) : user options
		Return
			(string) : formatted link for fetching
		"""
		q = self.base_url
		if "query" in self.options:
			q = "{}jobs?q={}".format(q, self.options['query'])
		else:
			print("query is required in options")
			return ""
		if "location" in self.options:
			q = "{}&l={}".format(q, self.options['location'])
		if "radius" in self.options:
			q = "{}&r={}".format(q, self.options['radius'])
		if "limit" in self.options:
			q = "{}&limit={}".format(q, self.options['limit'])
		return q

	def getPageContent(self, url):
		"""
		Getting HTML text of url page and bs4 parsign
		Args:
			url (string) : refers to requested URL
		return
			(string) : HTML string text of the requested page
		"""
		r = requests.get(url)
		if r.text:
			return BeautifulSoup(r.text, 'html.parser')

		return ""

	def parseJobsUrls(self, page_text):
		"""
		Parse metadata of different job based on sigle page content
		Args:
			page_text (bs4 HTML) : HTML soup content
		Return
			(dict) : job dict containing formatted jobs data  
		"""
		return [self.base_url[:-1]+link['href'] for link in page_text.find_all('a',{'class':'result'})]

	def generatePublicationDate(self, soup):
		"""
		Indeed provide us with how much days the job was posted
		The idea is to retrieve the number of days
		And generate corresponding date base on this
		Args:
			publication_string (string) : mentioning the nbr of days of the publication
		Return:
			(Date) if value < 30 else None
		"""
		try:
			publication_string = page_soup.select('span.jobsearch-HiringInsights-entry--text')[0].text
		except:
			return None
		n_days = re.findall(r'\d+', publication_string)
		if n_days:
			if "30" in n_days:
				# case when this post JOB was published more than 30 days, so we ignore
				return None
			return today - timedelta(days = n_days[0])
		return None

	def getResponsabilities(self, description):
		"""
		Generate responsabilities from description
		"""
		return ''

	def matchTechWithDescription(self, description):
		"""
		Match Tech languages with descrition
		Args
			description (string) : job post description
		Return
			(list) : list of categories ids if any else empty list
		"""
		try:
			return [tech for tech in techs if tech.name.lower() in description.lower()]
		except:
			return []

	def parseYearsExperience(self, description):
		"""
		extract how many years of experience is required for this job post
		Args:
			description (string) : refers to job description
		Return
			(int) : years of experience if any else 0
		"""
		n_years = re.findall(r'(\d)+\s*[year[s]|an[s]]', description)
		if n_years:
			return n_years[0]
		return 0

	def parseJobs(self):
		"""
		browse job links and extract metadata of each
		Args:
			link (links) : list of job links
		Return:
			list of dict : list of formatted dicts
		"""
		jobs = []
		existing_jobs_links = []
		processed = 0

		# removing already registred urls
		existing_jobs = Job.objects.filter(indeed_link__in = self.links)
		if existing_jobs:
			existing_jobs_links = [_job.indeed_link for _job in existing_jobs]

		for link_id, link in enumerate(self.links):
			#  skipping already processed links
			if link in existing_jobs_links:
				continue
				
			if self.task_id:
				current_task.update_state(task_id=self.task_id, state='PROGRESS',
	            meta={'current': link_id, 'total': len(self.links)})

			page_soup = self.getPageContent(link)
			description_list = page_soup.select('div#jobDescriptionText')
			description = description_list[0].text.strip() if description_list else ""
			jobs.append(dict(
					title = page_soup.select('h1.jobsearch-JobInfoHeader-title')[0].text.strip(),
					indeed_link = link,
					location = [div.find('div') for div in page_soup.select('div.jobsearch-JobInfoHeader-subtitle > div')][1].text,
					description = description,
					tech = self.matchTechWithDescription(description),
					experience_required = self.parseYearsExperience(description),
					company_name = page_soup.select('div.jobsearch-InlineCompanyRating div:nth-of-type(2)')[0].text,
					posted_date = self.generatePublicationDate(page_soup),
					responsabilities = self.getResponsabilities(description)
				))

			processed += 1
			if processed == MAX_QUERIES:
				# As mentioned in requirements, only MAX_QUERIES are allowed per 1 minute
				time.sleep(60)
				processed = 0

		self.jobs = jobs


	def parseMainURL(self):
		"""
		Generating corresponding request based on options
		Args:
			None
		Return
			(list) : list of Anchor links for all job posts
		"""
		return self.parseJobsUrls(self.getPageContent(self.query_link))

	def addJobsToDB(self):
		"""
		Populate DB with generated Job List
		"""
		for job in self.jobs:
			tech = job["tech"]
			del job["tech"]
			job_obj = Job(**job)
			job_obj.save()
			job_obj.tech.set(tech)


	def parse(self):
		# Generating link based on options
		self.query_link = self.generateMainLink()
		# Allowing search only if query is set
		if self.query_link:
			# Getting Job URLs
			self.links = self.parseMainURL()
			# Parsing jobs
			self.parseJobs()
			# populating DB
			self.addJobsToDB()
		else:
			raise Exception("Query is required for job search")
