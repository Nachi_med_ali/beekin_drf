# Scraper

Indeed Job Scraper

## Installation
- See installation in main README.md file

### Remote Server

A view page was created for this project to allow the scraping process to be fired based on options set by user. Please find the scrapping page under  **167.99.47.159:8080**

### Running

The process is set to run **async**. A **Redis queue** is set to fire the scraper jobs.

- To run the scraper manually:
    ```bash
    cd beekin_drf 
    source env/bin/activate
    python manage.py shell
    ```

- Inside the console, you need to import Scraper Class an run it
    ```python
    from scraper.scraper import IndeedParser
    parser = IndeedParser()
    parser.parse()
    ```

- A default ***options*** for Indeed Scraper. ***options*** is a dict mentioning needed informations for correctly filtering the jobs
    ```python
    DEFAULT_OPTIONS = {
        "language" : "fr", # (required) This refers to indeed country  
        "query" : "python", # (required) refers to keyword used for searching indeed 
        "location" : "paris", # (optional) refers to what city in chosen country to look for
        "radius" : 25, # (optional) refers to radius based on selected location
        "limit" : 10 # (optional) refers to how many results should be returned
    }
    ```

- The available version of indeed scraper allows **4** countries to be selected for ***language*** keyword in options. If ***language*** key is set to null or non exsiting country, scraper would stop the process
    ```python
    BASE_URLS = {
        "fr" : "https://indeed.fr/", # By setting language to "fr" in options, we'll get job results from France
        "en" : "https://indeed.com/",  # By setting language to "en" in options, we'll get job results from US
        "uk" : "https://uk.indeed.com/", # By setting language to "uk" in options, we'll get job results from UK
        "de" : "https://de.indeed.com/" # By setting language to "de" in options, we'll get job results from Germany
    }
    ```

## Scraper Class

### Attributes
<details>
<summary> IndeedParser.links (list)</summary> 

Defining jobs links returned by query results

</details>


<details>
<summary> IndeedParser.options (dict)</summary> 

Refering to options used for query. If not mentioned, it would be equal to default options

</details>


<details>
<summary> IndeedParser.base_url (string)</summary> 

Corresponding indeed URL based on language set by user (or default)

</details>


<details>
<summary> IndeedParser.query_link (string)</summary> 

URL built based on options to pass queries

</details>


<details>
<summary> IndeedParser.jobs (list)</summary> 

List of formatted jobs returned by querying *IndeedParser.query_link*

</details>


### Methods
<details>
<summary> IndeedParser.generateMainLink() </summary> 

Generate query URL for indeed

</details>

<details>
<summary> IndeedParser.getPageContent(LINK) </summary> 

Generate page SOUP HTML code 

</details>

<details>
<summary> IndeedParser.parseJobsUrls(SOUP_HTML_PAGE)</summary> 

Return FULL URL for jobs results from main search page of indeed

</details>

<details>
<summary> IndeedParser.generatePublicationDate(SOUP_HTML_PAGE)</summary> 

Retrieve Job Post publication date from page SOUP.

</details>

<details>
<summary> IndeedParser.getResponsabilities(SOUP_HTML_PAGE)</summary> 

***NOT IMPLEMENTED YET***

</details>

<details>
<summary> IndeedParser.matchTechWithDescription(PAGE_DESCRIPTION)</summary> 

Retrive all technologies used in job post description

</details>

<details>
<summary> IndeedParser.parseYearsExperience(PAGE_DESCRIPTION)</summary> 

REGEX parser for years of experience mentioned in job description

</details>


<details>
<summary> IndeedParser.parseJobs()</summary> 

Processing parser for all IndeedParser.links.

</details>


<details>
<summary> IndeedParser.parseMainURL()</summary> 

Scrape and parse main query page 

</details>

<details>
<summary> IndeedParser.addJobsToDB()</summary> 

Populate DB with resulted jobs from self.jobs

</details>

<details>
<summary> IndeedParser.parse()</summary> 

***MAIN*** fct 

</details>

