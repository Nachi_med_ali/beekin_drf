import configparser
import os.path


def load_secrets(filename='secrets.conf'):
    """Loads secrets from an external config file.

    The config file should be located in the home folder of the current process user.
    The file should be in a format readable by `configparser.ConfigParser` but without section header.
    """
    path = os.path.join(os.path.expanduser('~'), filename)

    with open(path, 'r') as f:
        contents = '[SECRETS]\n' + f.read()

    config = configparser.ConfigParser()
    config.read_string(contents)

    return config['SECRETS']
