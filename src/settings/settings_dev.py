"""
Settings for the development server

dev server url: http://167.99.47.159
"""

from src.conf import load_secrets
from pathlib import Path
from .settings_base import *

SECRETS = load_secrets()

SECRET_KEY = SECRETS['SECRET_KEY']

DEBUG = False

ALLOWED_HOSTS = ['167.99.47.159']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': SECRETS['DB_HOST'],
        'PORT': SECRETS['DB_PORT'],
        'NAME': SECRETS['DB_NAME'],
        'USER': SECRETS['DB_USER'],
        'PASSWORD': SECRETS['DB_PASSWORD'],
    }
}

BROKER_URL = SECRETS['BROKER_URL']
CELERY_RESULT_BACKEND = SECRETS['CELERY_RESULT_BACKEND']