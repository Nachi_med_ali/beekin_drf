from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import include


from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from rest_framework import permissions

# From DRF Swagger cli
# https://github.com/axnsan12/drf-yasg/
schema_view = get_schema_view(
    openapi.Info(
        title="Beekin Assignement",
        default_version="v1",
        description="Beekin API Documentation with DRF",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@email.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('', include('console.urls')),
    path('api/v1/', include('api.urls')),
    path('admin/', admin.site.urls),

    # Documentation
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    re_path(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
]
