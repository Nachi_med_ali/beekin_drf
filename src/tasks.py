"""
Defining celery tasks to be run in background
"""

from celery import shared_task
from scraper.scraper import IndeedParser

@shared_task(bind=True)
def scrapeIndeed(self, options = {}):
	"""
	Celery job for running indeedscraper
	"""
	task_id = self.request.id
	scraper = IndeedParser(options, task_id = task_id)
	scraper.parse()
