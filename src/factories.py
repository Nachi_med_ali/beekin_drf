import factory

from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyText, FuzzyDate
import datetime 

from api.models import (
    Job, Technology
)


class TechnologyFactory(DjangoModelFactory):
    class Meta:
        model = Technology

    name = FuzzyText()


class JobFactory(DjangoModelFactory):
    class Meta:
        model = Job

    indeed_link = FuzzyText()
    title = FuzzyText()
    location = FuzzyText()
    description = FuzzyText()
    # tech = factory.SubFactory(TechnologyFactory)
    experience_required = FuzzyText()
    company_name = FuzzyText()
    posted_date = FuzzyDate(datetime.date(2008, 1, 1))
    responsabilities = FuzzyText()