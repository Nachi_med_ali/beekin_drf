from django.shortcuts import render, redirect
from django.contrib import messages

from src.tasks import scrapeIndeed

def index(request):
    """
    Defining nested page for scrapping indeed
    """
    return render(
                request,
                "console/index.html",
                {},
            )

def scrape(request):
	"""
	Run scraping for selected preferences
	"""
	if request.method == "POST":
		q = request.POST.get("query")
		if not q:
			messages.error(request, 'Query is required')
			return redirect('console:index')
		options = {
			"query" : request.POST.get("query"),
			"language" : request.POST.get("language"),
			"location" : request.POST.get("location"),
			"radius" : request.POST.get("radius"),
			"limit" : request.POST.get("limit")
		}
		# Running Job in Redis Broker
		scrapeIndeed.delay(options)

		messages.success(request, 'Request successfully added to queue and would be processed shortly')
		return redirect('console:index')


		
		
