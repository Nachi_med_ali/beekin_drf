import os
from django.urls import reverse
from django.test import override_settings

from src.factories import JobFactory, TechnologyFactory
from api.models import Technology, Job

from django.test import TestCase
from scraper.scraper import IndeedParser

class TestMainURL(TestCase):

    def test_scraping_without_options(self):
        """
        Testing parser without options mentioned
        """
        a = IndeedParser()
        self.assertEqual(a.generateMainLink(), "https://indeed.fr/jobs?q=python&l=paris&r=25&limit=10")
    
    def test_scraping_with_options(self):
        """
        Testing parser with options mentioned
        """
        options = {
            "language" : "en",
            "query" : "python",
            "location" : "Nevada",
            "radius" : 30,
            "limit" : 20
        }
        a = IndeedParser(options)
        self.assertEqual(a.generateMainLink(), "https://indeed.com/jobs?q=python&l=Nevada&r=30&limit=20")

    def test_scraping_without_query_options(self):
        """
        Testing parser with options mentioned
        """
        options = {
            "language" : "en",
            "location" : "Nevada",
            "radius" : 30,
            "limit" : 20
        }
        a = IndeedParser(options)
        print(a.generateMainLink())
        self.assertEqual(a.generateMainLink(), "")

# class TestScraper(TestCase):
#     def test_scraping_job_urls(self):
#         """
#         Testing parser with options mentioned
#         """
#         options = {
#             "query" : "python",
#             "language" : "en",
#             "location" : "Nevada",
#             "radius" : 30,
#             "limit" : 2
#         }
#         a = IndeedParser(options)
#         a.parse()
#         self.assertEqual(len(a.jobs), options["limit"])

#         jobs = Job.objects.all()
#         self.assertEqual(jobs.count(), options["limit"])

#     def test_scraping_job_metadata(self):
#         """
#         Testing parser with job metadata
#         """
#         a = IndeedParser()
#         TEST_URL = "https://fr.indeed.com/voir-emploi?jk=7f246b193949b40d&tk=1fuid0fq5g3kl800&from=serp&vjs=3"
#         a.links = [TEST_URL]
#         a.parseJobs()
#         a.addJobsToDB()

#         jobs = Job.objects.all()
#         self.assertEqual(jobs.count(), 1)

#         job = jobs.first()
        
#         expected_result = {
#             "title" : "Data Analyst (F/H)",
#             "indeed_link" : TEST_URL,
#             "location" : "Paris (75)",
#             "company_name" : "Christian Dior Couture"
#         }
#         self.assertEqual(job.title, expected_result["title"])
#         self.assertEqual(job.indeed_link, expected_result["indeed_link"])
#         self.assertEqual(job.location, expected_result["location"])
#         self.assertEqual(job.company_name, expected_result["company_name"])

